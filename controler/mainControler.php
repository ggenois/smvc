<?php class mainControler
{
	public $core = false;

	function __construct(&$coreCi)
	{
		$this->core = $coreCi;
	}

    public function index()
    {
		$arrData = [];
		$arrData['config'] = $this->core->rConfig;
		
		//$arrReturn = $this->core->dsm_db("SELECT posts IN test (*)");
		
		$this->core->load('view','main/mainView',$arrData);
	}

	public function notFound() {
		echo 'not found';
	}
}
