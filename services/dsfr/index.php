<?php
	
	class dsfr {
		public $form_element = "upload";
		public $repo = "data";
		public $file_prefix = '';
		public $file_data_ext = 'data';
		public $result = array();
		public $iKey = 1;
		public $rKey = array();
		public $get_param = "q";
		public $key_param = "key";
		public $del_param = "d";
		
		public function do_upload()
		{
			$total = count($_FILES[$this->form_element]['name']);
			if($total > 0)
			{
				$this->result['files'] = array();
                $tmpFilePath = $_FILES['upload']['tmp_name'];
                if ($tmpFilePath != ""){
                    $uid = uniqid($this->file_prefix.time());
                    $newFilePath = "./".$this->repo."/" . $uid;
                    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                        file_put_contents($newFilePath.'.'.$this->file_data_ext,json_encode(array(
                            "Key" => (isset($_POST['key']) ? $_POST['key'] : 'public'), 
                            "time" => time(),
                            "filename"=>$_FILES['upload']['name'],
                            "type" => $_FILES['upload']['type'],
                            "size" => $_FILES['upload']['size']
                        )));
                        $this->result['iCode'] = 0;
                        $this->result['files'][] = $uid;
                    } else {
                        $this->result['iCode'] = 1;
                        $this->result['sMessage'] = "Intenal Error : Could not move the file to final destination from [$tmpFilePath] to [$newFilePath].";
                    }
                } else {
                    $this->result['iCode'] = 1;
                    $this->result['sMessage'] = "Error: File name empty.";
                }
				
			}
			else
			{
				$this->result['iCode'] = 1;
				$this->result['sMessage'] = "Error: No file to upload.";
			}
		
		}
		public function init()
		{
		    $this->rKey = json_decode(file_get_contents("./api_keys.json"), true);
			if ($this->rKey == NULL)
		    {
		        echo json_encode(array("iCode" => -1, "message" => "Internal error: Error reading api_keys.json"));
		        return (NULL);
		    }
		    if(isset($this->rKey["keys"]))
			    $this->rKey = $this->rKey["keys"];
			else
			{
			    echo json_encode(array("iCode" => -1, "message" => "Internal error: Error reading api_keys.json. Syntax error"));
			    return (NULL);
			}
			if(isset($_FILES[$this->form_element]))
			{
				if ($this->iKey) {
					if(isset($_POST[$this->key_param]))
					{
						if(in_array($_POST[$this->key_param], $this->rKey))
						{
							$this->do_upload();
						}
						else
						{
							$this->result['iCode'] = 1;
							$this->result['sMessage'] = "Error: Invalid key recieved.";
						}
					} else {
						$this->result['iCode'] = 1;
						$this->result['sMessage'] = "Error: No key recieved.";
					}
				} else {
					$this->do_upload();
				}
				header('Content-Type: application/json');
				echo json_encode($this->result);
			}
			else if (isset($_GET[$this->get_param]))
			{
				$name = './'.$this->repo."/".$_GET[$this->get_param];
				$fp = fopen($name, 'rb');
				if($fp) {
					$imageData = json_decode(file_get_contents($name.'.'.$this->file_data_ext),true);
					header("Content-Type: ".$imageData['type']);
					header("Content-Length: " . $imageData['size']);
					fpassthru($fp);
				} else {
					header('Content-Type: application/json');
					$this->result['iCode'] = 1;
					$this->result['iMessage'] = "Error: File not found.";
					echo json_encode($this->result);
				}
			}
			else if(isset($_GET[$this->del_param]) && isset($_GET[$this->key_param]))
			{
                if(in_array($_GET[$this->key_param], $this->rKey))
                {
                    $name = './'.$this->repo."/".$_GET[$this->del_param];
                    if (unlink($name) && unlink($name.'.data'))
                    {
                        $this->result['iCode'] = 0;
                        $this->result['sMessage'] = "File_deleted";
                    }
                    else
                    {
                        $this->result['iCode'] = 1;
                        $this->result['sMessage'] = "Error: Unable to delete the file from disk";
                    }
                    echo json_encode($this->result);
                }
                else
                {
                    $this->result['iCode'] = 1;
                    $this->result['sMessage'] = "Error: Invalid key recieved.";
                    echo json_encode($this->result);
                }
			}
			else
			{
				header('Content-Type: application/json');
				$this->result['iCode'] = 1;
				$this->result['iMessage'] = "dsfr. no query recieved.";
				$this->result['get'] = $_GET;
				echo json_encode($this->result);
			}
		}
	}
	$dsfr = new dsfr;
	$dsfr->init();
