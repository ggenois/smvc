<?php
/*
    Copyright 2017-2020 Gabriel Genois

    This file is part of SMVC (Small Module View Controler).

    SMVC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMVC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMVC.  If not, see <http://www.gnu.org/licenses/>.

*/
  class core {
	// FolderLevel of the BASE_URL. (Ex.: /:0, /e/:1. /e/s/:2.)
	public $iSubFolder = 0;
	private $config_path = './config.json';
	public $rConfig = array(
		"iSubFolder"		=> 0,
		"sBaseUrl"			=> '/',
		"dbconnector"		=> 0,
		"sDsmuri"			=> false,
		"sDsmkey"			=> "",
		"sDsfruri"			=> false,
		"sDsfrkey"			=> "",
		"sDsmname"			=> "",
		"sBlogName"			=> "Sans titre",
		"sBlogDescription"	=> "",
		"sBlogLogo"			=> "",
		"iPublished"		=> 1
	);

	private $ctrl = array();

	function __construct()
	{
		session_start();
		$this->load_config();
		define("BASE_URL", $this->rConfig['sBaseUrl']);
		define("BLOG_NAME", $this->rConfig['sBlogName']);
		$this->load('helper','headerHelper');
		$this->controlerBridge();
	}

	public function load_config()
	{
		$rLocalConfig = json_decode(
			file_get_contents($this->config_path),
			true
		);
		foreach ($rLocalConfig as $key => $value) {
			if (isset($this->rConfig[$key])) {
				$this->rConfig[$key] = $value;
			}
		}
		$this->iSubFolder = $this->rConfig['iSubFolder'];
		if ($this->rConfig["sDsmuri"] === false) {
			$this->rConfig["sDsmuri"] = $this->rConfig["sBaseUrl"] .
				"services/dsm";
		}
		if ($this->rConfig["sDsfruri"] === false) {
			$this->rConfig["sDsfruri"] = $this->rConfig["sBaseUrl"] .
				"services/dsfr";
		}
	}

    public function dsm_db($sQuery)
    {
		$sUrl =
			$this->rConfig['sDsmuri'].
			'/?key='.$this->rConfig['sDsmkey'].
			'&query='.base64_encode($sQuery);
		$context = stream_context_create(
			array('http'=>array('method'=>"GET"))
		);
		$jsonData = json_decode(
			file_get_contents($sUrl, false, $context), true
		);
		return  ($jsonData);
    }

	public function load($sMode,$sPath,$arrData = false)
	{
		if($arrData)
		{
			if(is_array($arrData))
			{
				extract($arrData);
			}
		}
		switch ($sMode)
		{
			case 'view':
			case 'helper':
			case 'model':
				require(realpath('').'/'.$sMode.'/'.$sPath.'.php');
				break;
			default:
				return false;
				break;
		}
	}

	public function getControllers ()
	{
		$d = dir("./controler");
		if ($d == false)
			die("Error when loading controller list.");
		$arrList = array();
		while (false !== ($entry = $d->read()))
		{
			$arrList[strstr(
					$entry, 'Controler.php', true
				)] = strstr($entry, '.php', true);
		}
		$d->close();
		return ($arrList);
	}

	public function controlerBridge()
	{
		require('controler/mainControler.php');
		$arrControlerReference = $this->getControllers();
		if($this->getPageSection())
		{
			if(isset($arrControlerReference[$this->getPageSection()]))
			{
				if($this->getPageSection() !== 'main')
				{
					require(
						'controler/'.
						$arrControlerReference[$this->getPageSection()].
						'.php'
					);
				}
				if(class_exists(
					$arrControlerReference[$this->getPageSection()])
				)
				{
					$this->ctrl[
						$arrControlerReference[$this->getPageSection()]
					] = new $arrControlerReference[
						$this->getPageSection()
					]($this);
				}
				else
				{
					exit('System failure. Unable to get page section.');
				}
				if(
					$this->getPageSection(1) && method_exists(
						$arrControlerReference[
							$this->getPageSection()
						],$this->getPageSection(1)
					)
				)
				{
					$a1 = false;
					if($this->getPageSection(2))
					{
						$a1 = $this->getPageSection(2);
					}
					$this->ctrl[
						$arrControlerReference[$this->getPageSection()]
					]->{$this->getPageSection(1)}($a1);
				}
				else
				{
					$a1 = false;
					if($this->getPageSection(1))
					{
						$a1 = $this->getPageSection(1);
					}
					$this->ctrl[
						$arrControlerReference[$this->getPageSection()]
					]->index($a1);
				}
			}
			else
			{
				mainControler::notFound();
			}
		}
		else
		{
			if(class_exists('mainControler'))
			{
				$this->ctrl['mainControler'] =  new mainControler($this);
				if($this->getPageSection() == 'mainControler')
				{
					if($this->getPageSection(1))
					{
						$this->ctrl[
							'mainControler'
						]->{$this->getPageSection(1)}();
					}
					else
					{
						$this->ctrl['mainControler']->index();
					}
				}
				else
				{
					$this->ctrl['mainControler']->index();
				}
			}
			else
			{
				exit('system offline. Sorry.');
			}
		}
	}
	public function getPageSection($iIndex = 0)
	{
		$path = ltrim($_SERVER['REQUEST_URI'], '/');
		$elements = explode('/', $path);
		if(isset($elements[$this->iSubFolder + $iIndex]))
		{
			$value = explode('?', $elements[$this->iSubFolder + $iIndex]);
			return $value[0];
		}
		else
		{
			return false;
		}
    }
}
